package com.netflix.hystrix.crazyit.run;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

public class ThreadCommandA extends HystrixCommand<String> {

	public ThreadCommandA() {
	    super(HystrixCommandGroupKey.Factory.asKey("ThreadCommandA"));
	}

	@Override
	protected String run() throws Exception {
		System.out.println("这是 ThreadCommandA");
		return "success";
	}

}
