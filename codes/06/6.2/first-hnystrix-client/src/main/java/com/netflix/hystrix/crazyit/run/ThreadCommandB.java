package com.netflix.hystrix.crazyit.run;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

public class ThreadCommandB extends HystrixCommand<String> {

	public ThreadCommandB() {
	    super(HystrixCommandGroupKey.Factory.asKey("ThreadCommandB"));
	}

	@Override
	protected String run() throws Exception {
		Thread.sleep(500);
		System.out.println("这是 ThreadCommandB");
		return "success";
	}

}
