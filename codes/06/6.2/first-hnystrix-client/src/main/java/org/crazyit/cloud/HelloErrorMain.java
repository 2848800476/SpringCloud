package org.crazyit.cloud;

public class HelloErrorMain {

	public static void main(String[] args) {
		// 请求异常的服务
		String normalUrl = "http://localhost:8080/errorHello";
		HelloCommand command = new HelloCommand(normalUrl);
		String result = command.execute();
		System.out.println("请求异常的服务，结果：" + result);
	}
}
