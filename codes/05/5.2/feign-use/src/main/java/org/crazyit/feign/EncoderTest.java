package org.crazyit.feign;

import org.crazyit.feign.PersonClient.Person;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;

/**
 * 运行主类
 * @author 杨恩雄
 *
 */
public class EncoderTest {

	public static void main(String[] args) {
		// 获取服务接口
		PersonClient personClient = Feign.builder()
				.encoder(new GsonEncoder())
				.target(PersonClient.class, "http://localhost:8080/");
		// 创建参数的实例
		Person person = new Person();
		person.id = 1;
		person.name = "Angus";
		person.age = 30;
		String response = personClient.createPerson(person);
		System.out.println(response);
	}
}
